package ru.t1.godyna.tm.exception.field;

public final class DescriptionEmptyException extends AbsractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description not found...");
    }

}
