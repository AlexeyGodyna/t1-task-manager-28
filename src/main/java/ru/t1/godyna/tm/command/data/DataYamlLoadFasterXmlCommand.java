package ru.t1.godyna.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.Domain;
import ru.t1.godyna.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-load-yaml";

    @Getter
    @NotNull
    private final String description = "Load data from yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}
