package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.api.repository.IRepository;
import ru.t1.godyna.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
